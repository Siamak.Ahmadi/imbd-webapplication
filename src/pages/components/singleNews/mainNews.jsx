import"./news.css"
import React,{useState,useEffect} from "react";
import NewsCard from './Component/newsCard'


export default function MainNews() {


  const [newsCard, setNewsCard] = useState([]);

  useEffect(()=>{
    fetch('http://localhost:3000/movieNews')
    .then(Response=> Response.json())
    .then(newsCard=>setNewsCard(newsCard));

  }
  ,[]);


  const NewsCardDetail = newsCard.map(detail=> <NewsCard Title={detail.Title} Summary={detail.Summary} Img={detail.Img} Description={detail.Description} IMDb_RATING={detail.IMDb_RATING} Production_Year={detail.Production_Year} Time={detail.Time} Genres={detail.Genres} Actors={detail.Actors}/>) ;


  return (
    <>
      {NewsCardDetail}
    </>
  );
}
