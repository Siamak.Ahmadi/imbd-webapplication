import "../news.css";
import React from "react";
import { Link } from "react-router-dom";
export default function newsCard({ Img, Title, Summury, Time, Slug }) {
  return (
    <Link to={`/singleNews/${Slug}`} key={Slug}>
      <div className="flex flex-row  mt-5 w-ful">
        <img className="w-24 h-24 rounded-xl object-cover " src={Img} alt="" />

        <div className="ml-2">
          <div className="flex justify-between">
            <h1 className="font-bold mainFont">{Title}</h1>
            <span className="font-bold">{Time}</span>
          </div>
          <div>
            <p>{Summury}</p>
          </div>
        </div>
      </div>
    </Link>
  );
}
