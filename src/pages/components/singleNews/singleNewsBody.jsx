import "./news.css";
import React,{useEffect,useState} from "react";
import { useParams } from 'react-router-dom'

export default function SingleNewsBody() {


    const Slug = useParams().slug;

    const [newsContent, setnewsContent] = useState([]);

    useEffect(()=>{
      fetch(`http://localhost:3000/movieNews?Slug=${Slug}`)
      .then(Response=> Response.json())
      .then(newsContent=>setnewsContent(newsContent));
      
    },[Slug]);



    const NewsBody = newsContent.map(detail=> <div><div className="m-10  rounded-xl h-52"><img src={detail.Img} className="h-full w-full  rounded-xl object-cover" alt=""/></div><div className="m-10"><div className="flex justify-between"><h1 className="text-xl font-bold mainFont ">{detail.Title}</h1><h1 className="text-sl mainFont ">{detail.Time}</h1></div><div className="mainFont mt-8 text-sl">{detail.Body}</div></div></div>) ;
    
    return (
        <div className="">

            <div className="flex items-center">
                <div className='backIcon iconBackground w-24 h-24'onClick={() => {window.history.back()}}></div>

                <div className='mainFont text-xl font-bold ml-20 '>
                    News
                </div>
            </div>
        
            {NewsBody}



        </div>
    )
}
