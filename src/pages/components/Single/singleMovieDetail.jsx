import "./Style.css";
import React,{useState} from "react";

export default function MovieSinglePage({Title,Img,Description,Time,IMDb_RATING,Production_Year,Genres,Actors}) {



  const [islike, setLike] = useState(true);
  const [isTicket, setTicket] = useState(true)


  
  const genresMap=Genres.map(detail=> <div className="w-auto h-10 mr-3 rounded-full GenresColor text-center p-2 ">
  <div className="font-bold mainFont">{detail}</div>
  </div> );

  const actorsMap=Actors.map(detail=>
  <div className="mt-3 mr-5 flex flex-col   content-center overflow-scroll overFlowWrapNone">
  <img src={detail.Img} alt={detail.Name} className=" w-20 h-20 rounded-full object-cover"/>
  <p className="text-md font-bold">{detail.Name}</p>
  <p>{detail.Position}</p></div>);

  
  
  return (
    <div>
      <div className="flex justify-between  w-full sticky top-0  ">
        <div className="backIcon iconBackground w-24 h-24"  onClick={() => {window.history.back()}}>
        </div>

        <div  onClick={()=>{islike ? setLike(false) : setLike(true)}} className={ islike ? "heartIconBold iconBackground w-24 h-24" :  "heartIcon iconBackground w-24 h-24"}></div>
        
      </div>

      <div>
        <img src={Img} alt={Title} className=" w-auto movieIMG" />
      </div>
      <div className="h-3/4 background mt-60 ">
        <div className="flex flex-col items-center ">
          <h2 className="text-3xl font-bold mt-24 mainFont ">{Title}</h2>
          <div className="flex justify-between">
            <h6 className="mainFont">{Production_Year}</h6>
            <h6 className="mr-3 ml-3">.</h6>
            <h6 className="mainFont">{Time}</h6>
          </div>
          <div className="flex items-center ">
              <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M7.67752 0.927052C7.97687 0.00574088 9.28028 0.0057404 9.57963 0.927051L10.7903 4.65318C10.9242 5.0652 11.3082 5.34416 11.7414 5.34416H15.6593C16.628 5.34416 17.0308 6.58378 16.247 7.15318L13.0774 9.45605C12.7269 9.7107 12.5803 10.1621 12.7141 10.5741L13.9248 14.3002C14.2242 15.2215 13.1697 15.9876 12.386 15.4182L9.21636 13.1154C8.86587 12.8607 8.39127 12.8607 8.04079 13.1154L4.87115 15.4182C4.08744 15.9876 3.03296 15.2215 3.33231 14.3002L4.543 10.5741C4.67688 10.1621 4.53022 9.7107 4.17973 9.45605L1.0101 7.15318C0.226385 6.58378 0.62916 5.34416 1.59788 5.34416H5.51577C5.94899 5.34416 6.33295 5.0652 6.46682 4.65318L7.67752 0.927052Z" fill="#FD9F00"/>
              </svg>
              <p className="mainFont">{IMDb_RATING}</p>
          </div>
        </div>


        <div className="m-5">
          <div className="flex  w-full overflow-scroll overFlowWrapNone">
          {genresMap}
          </div>

          <div className="mt-5">
            <h1 className="text-2xl font-bold mainFont">Summary</h1>
            <p className="mt-4 mainFont">{Description}</p>
          </div>

          <div className="mt-5">
            <h1 className="text-2xl font-bold mainFont">Cast</h1>
            <div className="flex ">
              {actorsMap}
            </div>
          </div>

    
          <div className="mt-5 sticky bottom-0 w-full flex justify-center ">
              
            <button  className={ isTicket ? "p-3 w-60 rounded-xl text-lg font-semibold mainFont secoundryColor " :  "p-3 w-60 rounded-xl text-lg font-semibold mainFont border-solid borderColor border-2 mainColor"} onClick={()=>{isTicket ? setTicket(false) : setTicket(true)}}>Buy Ticket</button>
          </div>
        </div>
      </div>
    </div>
  );
}
