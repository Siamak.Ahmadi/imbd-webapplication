import "./Style.css"
import React ,{useEffect} from  'react'
import { gsap } from "gsap";

export default function Loading() {


    useEffect(()=>{
        gsap.to(".Loading",0.9,{opacity:1}).yoyo(true).repeat(-1);

    })
  
    return (
        <>
            <div className="Loading">
                <div className="secoundryColor w-36 h-32 rounded-xl"></div>
                <div className="secoundryColor w-22 h-2 mt-3 rounded-xl"></div>
                <div className="secoundryColor w-24 h-2 mt-3 rounded-xl"></div>
                <div className="secoundryColor w-28 h-2 mt-3 rounded-xl"></div>
            </div>
            <div className="Loading ml-5">
                <div className="secoundryColor w-36 h-32 rounded-xl"></div>
                <div className="secoundryColor w-22 h-2 mt-3 rounded-xl"></div>
                <div className="secoundryColor w-24 h-2 mt-3 rounded-xl"></div>
                <div className="secoundryColor w-28 h-2 mt-3 rounded-xl"></div>
            </div>
            <div className="Loading ml-5">
                <div className="secoundryColor w-36 h-32 rounded-xl"></div>
                <div className="secoundryColor w-22 h-2 mt-3 rounded-xl"></div>
                <div className="secoundryColor w-24 h-2 mt-3 rounded-xl"></div>
                <div className="secoundryColor w-28 h-2 mt-3 rounded-xl"></div>
            </div>
        </>
    )
}
