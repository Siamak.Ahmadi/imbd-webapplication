import './header.css'
import React,{useState} from "react";
import { gsap } from "gsap";


export default function Header() {

    const [isToggle, isSetToggle] = useState(false);

    

    return (
        <div className='flex justify-between mainColor header  '>


            <div>
                <div className='mainFont'>Welcome</div>
                <div className='mainFont font-bold'>Siamak Ahmadi</div>
            </div>
            <div onClick={()=>{isToggle ?  isSetToggle(false) : isSetToggle(true);  gsap.to(".popup",.2,{opacity:1}).yoyo(true); }}>
                <img className='w-12 h-12 rounded-full object-cover' src='https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' alt="" />
            </div>

            <div className={isToggle ? "w-3/5 h-36  absolute top-16 right-6 customRounded popUpColor popup" :"w-3/5 h-36  absolute popup top-16 right-6 customRounded popUpColor hidden"}>
                <ul className='p-5 '>
                    <li className='mainFont'>Profile Infro</li>
                    <li className='mainFont'>Setting</li>
                    <li className='mainFont'>Log Out</li>
                </ul>
            </div>

        </div>
    )
}
