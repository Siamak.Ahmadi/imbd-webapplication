import React, { useEffect, useState } from "react";
import "./Style/movie.css";
import MovieCard from "./components/movieCard";
import Loading from "./components/Loading/main";
import { Link } from "react-router-dom";
export default function Movies() {
  const [movies, setmovies] = useState([{}]);

  useEffect(() => {
    fetch("http://localhost:3000/recentlyMovies")
      .then((Response) => Response.json())
      .then((movies) => setmovies(movies));
  }, []);

  const moviedetail = movies.map((detail) => (
    <MovieCard
      title={detail.Title}
      slug={detail.Slug}
      image={detail.Img}
      caption={detail.Summary}
    />
  ));

  return (
    <div>
      <div className="rounded-full w-full h-12 flex ">
        <input
          type="text"
          className=" searchInputBg w-full h-full rounded-full  border-solid border-2 border-blue-600"
          placeholder="Type Movie Name"
        />
      </div>

      <div>
        <div className="flex justify-between mt-4 items-baseline ">
          <h1 className="sm:text-lg mainFont font-semibold">Recently Movies</h1>
          <Link to="moremovie">
            <span className="sm:text-sm mainFont text-blue-600">See More</span>
          </Link>
        </div>

        <div className=" flex scroll w-full mt-4">
          {!movies.length ? <Loading /> : <>{moviedetail}</>}
        </div>
      </div>

      <div>
        <div className="flex justify-between mt-4 items-baseline ">
          <h1 className="text-lg mainFont font-semibold">Trending Movies</h1>
          <span className="text-sm mainFont text-blue-600">See More</span>
        </div>

        <div className=" flex scroll w-full mt-4">
          <Loading />
        </div>
      </div>

      <div>
        <div className="flex justify-between mt-4 items-baseline ">
          <h1 className="text-lg mainFont font-semibold">Upcoming Movies</h1>
          <span className="text-sm mainFont text-blue-600">See More</span>
        </div>
        <div className=" flex scroll w-full mt-4">{<Loading />}</div>
      </div>
    </div>
  );
}
