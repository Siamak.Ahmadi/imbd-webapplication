// import Movies from "./movies";
// import News from "./news";
// import Shows from "./shows";
import { Outlet } from "react-router-dom";
import Navigation from "./components/navigation";
import Header from "./components/header";

function Main() {
  return (
    <div className="">
      <div className="m-4">
        <Header />
        <div className="mt-6">
          <Outlet/>
        </div>
      </div>

      <div className="fixed bottom-0 w-full h-14  mainColor  lg:w-60 lg:left-1/3 lg:rounded-full   lg:bottom-2 ">
        <Navigation />
      </div>
    </div>
  );
}

export default Main;
