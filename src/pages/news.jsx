import React,{useEffect,useState} from "react";
import "./components/singleNews/news.css";
import MainNews from "./components/singleNews/Component/mainNewsComponent";
import NewsCard from "./components/singleNews/Component/newsCard";

export default function News() {

  const [newsCard, setNewsCard] = useState([]);


  useEffect(()=>{
    fetch('http://localhost:3000/movieNews')
    .then(Response=> Response.json())
    .then(newsCard=>setNewsCard(newsCard));
  }
  ,[]);


  const newsCardDetail = newsCard.map(detail=> <NewsCard Img={detail.Img} Title={detail.Title} Slug={detail.Slug} Summury={detail.Summury} Time={detail.Time} />) ;

  return (
    <div className="">
      <MainNews  />

     {newsCardDetail}
      
    </div>
  );
}
