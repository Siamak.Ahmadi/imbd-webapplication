import React from "react";
import "./Style/movie.css";
// import MovieCard from "./components/movieCard";
import Loading from "./components/Loading/main";
export default function Movies() {
  return (
    <div>
      <div className="rounded-full w-full h-12 flex ">
        <input
          type="text"
          className=" searchInputBg w-full h-full rounded-full  border-solid border-2 border-blue-600"
          placeholder="Type Movie Name"
        />
      </div>
      <div>
        <div className="flex justify-between mt-4 items-baseline ">
          <h1 className="sm:text-lg font-semibold">Today TV Shows</h1>
          <span className="sm:text-sm text-blue-600">See More</span>
        </div>
        <div className=" flex scroll w-full mt-4">{<Loading />}</div>
      </div>
      <div>
        <div className="flex justify-between mt-4 items-baseline ">
          <h1 className="text-lg font-semibold">Popular TV Shows</h1>
          <span className="text-sm text-blue-600">See More</span>
        </div>
        <div className=" flex scroll w-full mt-4">{<Loading />}</div>
      </div>
      <div>
        <div className="flex justify-between mt-4 items-baseline ">
          <h1 className="text-lg font-semibold">Trending TV Shows</h1>
          <span className="text-sm text-blue-600">See More</span>
        </div>
        <div className=" flex scroll w-full mt-4">{<Loading />}</div>
      </div>
      <div>
        <div className="flex justify-between mt-4 items-baseline ">
          <h1 className="text-lg font-semibold">Recently TV Shows</h1>
          <span className="text-sm text-blue-600">See More</span>
        </div>
        <div className=" flex scroll w-full mt-4">{<Loading />}</div>
      </div>
    </div>
  );
}
