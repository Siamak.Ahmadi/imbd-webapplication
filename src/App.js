import "./App.css";
import { Route, Routes } from "react-router-dom";
import Layout from "./pages/layout"
import SinglePage from "./pages/components/Single/mainSingle";
import SingleNews from "./pages/components/singleNews/singleNewsBody"
import Movies from "./pages/movies";
import Shows from "./pages/shows";
import News from "./pages/news"
function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route path="/" element={<Movies />} />
          <Route path="shows" element={<Shows />} />
          <Route path="news" element={<News />} />
        </Route>
        <Route path="single/:slug" element={<SinglePage />} />
        <Route path="singleNews/:slug" element={<SingleNews/>} />
      </Routes>
    </>
  );
}

export default App;
